/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementTipekayu;
import penjulan_penggajian_jaya_abadi.koneksi.koneksi;
import penjulan_penggajian_jaya_abadi.model.tipekayu;

/**
 *
 * @author Ujang Jeprut
 */
public class daoTipekayu implements ImplementTipekayu {
    Connection connection;
    final String insert="INSERT INTO t_tipe_kayu (kode_kayu,jenis_kayu,harga) VALUES (?,?,?);";
    final String update = "UPDATE t_tipe_kayu set jenis_kayu=?, harga=? where kode_kayu=? ;";
    final String delete = "DELETE FROM t_tipe_kayu where kode_kayu=? ;";
    final String select = "SELECT * FROM t_tipe_kayu;";
    final String caridata = "SELECT * FROM t_tipe_kayu WHERE kode_kayu LIKE ? OR jenis_kayu LIKE ? ";
    
    public daoTipekayu() {
        connection = koneksi.connection();
    }
    
    public void insert(tipekayu b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getKode_kayu());
            statement.setString(2, b.getJenis_kayu());
            statement.setDouble(3, b.getHarga());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            //ResultSet rs = statement.getGeneratedKeys();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Kode Kayu sudah ada, Gagal menyimpan !",
                    "Information",JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void update(tipekayu b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getJenis_kayu());
            statement.setDouble(2, b.getHarga());
            statement.setString(3, b.getKode_kayu());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(String kode_kayu) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setString(1, kode_kayu);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<tipekayu> getALL() {
        List<tipekayu> lb = null;
        try {
            lb = new ArrayList<tipekayu>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                tipekayu b = new tipekayu();
                b.setKode_kayu(rs.getString("kode_kayu"));
                b.setJenis_kayu(rs.getString("jenis_kayu"));
                b.setHarga(rs.getDouble("harga"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoTipekayu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<tipekayu> getCariData(String data) {
        List<tipekayu> lb = null;
        try {
            lb = new ArrayList<tipekayu>();
            PreparedStatement st = connection.prepareStatement(caridata);
            st.setString(1, "%" + data + "%");
            st.setString(2, "%" + data + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                tipekayu b = new tipekayu();
                b.setKode_kayu(rs.getString("kode_kayu"));
                b.setJenis_kayu(rs.getString("jenis_kayu"));
                b.setHarga(rs.getDouble("harga"));

                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoTipekayu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
}
