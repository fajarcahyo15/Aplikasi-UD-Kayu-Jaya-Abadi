/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAOImplement;

import java.util.List;
import penjulan_penggajian_jaya_abadi.model.*;

/**
 *
 * @author Ujang Jeprut
 */
public interface ImplementTipekayu {
    public void insert(tipekayu b);
    
    public void update(tipekayu b);

    public void delete(String kode_kayu);

    public List<tipekayu> getALL();

    public List<tipekayu> getCariData(String data);
}
