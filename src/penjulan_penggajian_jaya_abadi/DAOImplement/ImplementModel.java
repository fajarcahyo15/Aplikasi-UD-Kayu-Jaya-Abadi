/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAOImplement;
import java.util.List;
import penjulan_penggajian_jaya_abadi.model.*;

/**
 *
 * @author hikira1
 */
public interface ImplementModel {
    public void insert(model b);
    
    public void update(model b);

    public void delete(String kode_model);

    public List<model> getALL();

    public List<model> getCariData(String data);
}
