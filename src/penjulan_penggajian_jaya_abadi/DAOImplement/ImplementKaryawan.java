/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAOImplement;

import java.util.List;
import penjulan_penggajian_jaya_abadi.model.*;

/**
 *
 * @author Ujang Jeprut
 */
public interface ImplementKaryawan {
    public void insert(karyawan b);
    
    public void update(karyawan b);

    public void delete(String id_karyawan);

    public List<karyawan> getALL();

    public List<karyawan> getCariData(String data);
    
    public List<karyawan> getLogin(String username, String password);
    
    public void daftar(karyawan b);
}
