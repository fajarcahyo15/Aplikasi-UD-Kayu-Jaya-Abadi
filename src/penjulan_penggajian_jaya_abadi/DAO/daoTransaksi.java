/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementTransaksi;
import penjulan_penggajian_jaya_abadi.koneksi.koneksi;
import penjulan_penggajian_jaya_abadi.model.transaksi;

/**
 *
 * @author Ujang Jeprut
 */
public class daoTransaksi implements ImplementTransaksi {
    Connection connection;
    final String insert="INSERT INTO t_transaksi (no_transaksi,nama_pelanggan,alamat,no_telpon,tanggal,total_harga,bayar,sisa) "
            + "VALUES (?,?,?,?,?,?,?,?);";
    final String update = "UPDATE t_transaksi set nama_pelanggan=?, alamat=?, no_telpon=?, tanggal=?, total_harga=?, bayar=?, sisa=? "
            + "where no_transaksi=? ;";
    final String delete = "DELETE FROM t_transaksi where no_transaksi=? ;";
    final String select = "SELECT * FROM t_transaksi;";
    final String caridata = "SELECT * FROM t_transaksi where no_transaksi like ? or nama_pelanggan like ? or no_telpon";
    
    public daoTransaksi() {
        connection = koneksi.connection();
    }
    
    public void insert(transaksi b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getNo_transaksi());
            statement.setString(2, b.getNama_pelanggan());
            statement.setString(3, b.getAlamat());
            statement.setString(4, b.getNo_telp());
            statement.setString(5, b.getTanggal());
            statement.setDouble(6, b.getTotal_harga());
            statement.setDouble(7, b.getBayar());
            statement.setDouble(8, b.getSisa());
            
            statement.executeUpdate();
            /*JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);*/
            //ResultSet rs = statement.getGeneratedKeys();
            
        } catch (SQLException ex) {
            update(b);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void update(transaksi b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getNama_pelanggan());
            statement.setString(2, b.getAlamat());
            statement.setString(3, b.getNo_telp());
            statement.setString(4, b.getTanggal());
            statement.setDouble(5, b.getTotal_harga());
            statement.setDouble(6, b.getBayar());
            statement.setDouble(7, b.getSisa());
            statement.setString(8, b.getNo_transaksi());
            
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(String no_transaksi) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setString(1, no_transaksi);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<transaksi> getALL() {
        List<transaksi> lb = null;
        try {
            lb = new ArrayList<transaksi>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                transaksi b = new transaksi();
                b.setNo_transaksi(rs.getString("no_transaksi"));
                b.setNama_pelanggan(rs.getString("nama_pelanggan"));
                b.setAlamat(rs.getString("alamat"));
                b.setNo_telp(rs.getString("no_telpon"));
                b.setTanggal(rs.getString("tanggal"));
                b.setTotal_harga(rs.getDouble("total_harga"));
                b.setBayar(rs.getDouble("bayar"));
                b.setSisa(rs.getDouble("sisa"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<transaksi> getCariData(String data) {
        List<transaksi> lb = null;
        try {
            lb = new ArrayList<transaksi>();
            PreparedStatement st = connection.prepareStatement(caridata);
            st.setString(1, "%" + data + "%");
            st.setString(2, "%" + data + "%");
            st.setString(3, "%" + data + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                transaksi b = new transaksi();
                b.setNo_transaksi(rs.getString("no_transaksi"));
                b.setNama_pelanggan(rs.getString("nama_pelanggan"));
                b.setAlamat(rs.getString("alamat"));
                b.setNo_telp(rs.getString("no_telpon"));
                b.setTanggal(rs.getString("tanggal"));
                b.setTotal_harga(rs.getDouble("total_harga"));
                b.setBayar(rs.getDouble("bayar"));
                b.setSisa(rs.getDouble("sisa"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
}
