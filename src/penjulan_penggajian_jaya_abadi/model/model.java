/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

/**
 *
 * @author hikira1
 */
public class model {
    private String kode_model;
    private String nama_model;

    public String getKode_model() {
        return kode_model;
    }

    public void setKode_model(String kode_model) {
        this.kode_model = kode_model;
    }

    public String getNama_model() {
        return nama_model;
    }

    public void setNama_model(String nama_model) {
        this.nama_model = nama_model;
    }
}
