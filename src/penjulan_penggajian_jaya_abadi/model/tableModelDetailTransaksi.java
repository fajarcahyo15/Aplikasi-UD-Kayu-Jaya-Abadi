/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ujang Jeprut
 */
public class tableModelDetailTransaksi extends AbstractTableModel {
    List<detail_transaksi> lb;

    public tableModelDetailTransaksi(List<detail_transaksi> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }


    @Override
    public int getRowCount() {
        return lb.size();
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nama Barang";
            case 1:
                return "Ukuran";
            case 2:
                return "Harga Satuan";
            case 3:
                return "Qty";
            case 4:
                return "Jumlah";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getNama_barang();
            case 1:
                return lb.get(row).getUkuran();
            case 2:
                return lb.get(row).getHarga_satuan();
            case 3:
                return lb.get(row).getQty();
            case 4:
                return lb.get(row).getJumlah();  
            default:
                return null;
        }
    }
    
    
}
