/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.controller;

import java.util.List;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAO.daoBarang;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementBarang;
import penjulan_penggajian_jaya_abadi.model.barang;
import penjulan_penggajian_jaya_abadi.model.tableModelBarang;
import penjulan_penggajian_jaya_abadi.model.teksModel;
import penjulan_penggajian_jaya_abadi.view.frm_barang;

/**
 *
 * @author Ujang Jeprut
 */
public class controllerBarang {
    frm_barang frame;

    ImplementBarang implBarang;
    List<barang> lb;
    
    public controllerBarang(frm_barang frame)
    {
        this.frame = frame;
        implBarang = new daoBarang();
        lb = implBarang.getALL();
    }
    
    //membuat kondisi default program saat terjadi aksi tertentu
    public void konfig_teks()
    {
        teksModel teks_1 = new teksModel(6);
        teksModel teks_2 = new teksModel(5);
        teksModel teks_3 = new teksModel(5);
        teksModel teks_4 = new teksModel(50);
        frame.getTxt_kodebarang().setDocument(teks_1);
        frame.getTxt_kodekayu().setDocument(teks_2);
        frame.getTxt_kodemodel().setDocument(teks_3);
        frame.getTxt_namabarang().setDocument(teks_4);
    }
    
    public void reset()
    {
        frame.getTxt_kodebarang().setText("");
        frame.getTxt_kodemodel().setText("");
        frame.getTxt_namamodel().setText("");
        frame.getTxt_kodekayu().setText("");
        frame.getTxt_jeniskayu().setText("");
        frame.getTxt_hargakayu().setText("");
        frame.getTxt_namabarang().setText("");
        frame.getTxt_hargabarang().setText("");
        frame.getTxt_caridata().setText("");
        
        frame.getTxt_kodebarang().setEnabled(false);
        frame.getTxt_kodemodel().setEnabled(false);
        frame.getTxt_namamodel().setEnabled(false);
        frame.getTxt_kodekayu().setEnabled(false);
        frame.getTxt_jeniskayu().setEnabled(false);
        frame.getTxt_hargakayu().setEnabled(false);
        frame.getTxt_namabarang().setEnabled(false);
        frame.getTxt_hargabarang().setEnabled(false);
        frame.getTxt_caridata().setEnabled(true);
        
        frame.getTxt_kodemodel().setEditable(false);
        frame.getTxt_kodekayu().setEditable(false);
        
        frame.getBtn_tambah().setEnabled(true);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(false);
        frame.getBtn_keluar().setEnabled(true);
    }
    
    //fungsi untuk tombol tambah
    public void tambah()
    {
        frame.getTxt_kodebarang().setEnabled(true);
        frame.getTxt_kodemodel().setEnabled(true);
        frame.getTxt_kodekayu().setEnabled(true);
        frame.getTxt_namabarang().setEnabled(true);
        frame.getTxt_hargabarang().setEnabled(true);
        frame.getTxt_caridata().setText("");
        
        frame.getTxt_caridata().setEnabled(false);
        
        frame.getTxt_kodebarang().requestFocus();
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
    public void tabel_click()
    {
        frame.getTxt_kodebarang().setEnabled(false);
        frame.getTxt_namabarang().setEnabled(true);
        frame.getTxt_hargabarang().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(true);
        frame.getBtn_hapus().setEnabled(true);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
    //menampilkan isi data ke dalam tabel
    public void isiTable()
    {
        lb = implBarang.getALL();
        tableModelBarang tmb = new tableModelBarang(lb);
        frame.getT_barang().setModel(tmb);
    }
    
    public void isiField(int row)
    {
        frame.getTxt_kodebarang().setText(lb.get(row).getKode_barang().toString());
        frame.getTxt_kodemodel().setText(lb.get(row).getKode_model().toString());
        frame.getTxt_namamodel().setText(lb.get(row).getNama_model().toString());
        frame.getTxt_kodekayu().setText(lb.get(row).getKode_kayu().toString());
        frame.getTxt_jeniskayu().setText(lb.get(row).getJenis_kayu().toString());
        frame.getTxt_hargakayu().setText(lb.get(row).getHarga_kayu().toString());
        frame.getTxt_namabarang().setText(lb.get(row).getNama_barang().toString());
        frame.getTxt_hargabarang().setText(lb.get(row).getHarga_barang().toString());
        
        frame.getTxt_kodekayu().setEnabled(true);
        frame.getTxt_kodemodel().setEnabled(true);
    }
    
    //fungsi untuk menyimpan data
    public void simpan()
    {
        if((!frame.getTxt_kodebarang().getText().trim().isEmpty()) && 
                (!frame.getTxt_kodemodel().getText().trim().isEmpty()) &&
                (!frame.getTxt_kodekayu().getText().trim().isEmpty()) &&
                (!frame.getTxt_hargabarang().getText().trim().isEmpty()) &&
                (!frame.getTxt_hargabarang().getText().trim().isEmpty()))
        {
            barang b = new barang();
            b.setKode_barang(frame.getTxt_kodebarang().getText());
            b.setKode_model(frame.getTxt_kodemodel().getText());
            b.setKode_kayu(frame.getTxt_kodekayu().getText());
            b.setNama_barang(frame.getTxt_namabarang().getText());
            b.setHarga_barang(Double.valueOf(frame.getTxt_hargabarang().getText()));
            
            implBarang.insert(b);
            
            
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    //fungsi untuk mengubah data
    public void ubah() 
    {
        if((!frame.getTxt_kodebarang().getText().trim().isEmpty()))
        {
            barang b = new barang();
            b.setKode_barang(frame.getTxt_kodebarang().getText());
            b.setKode_model(frame.getTxt_kodemodel().getText());
            b.setKode_kayu(frame.getTxt_kodekayu().getText());
            b.setNama_barang(frame.getTxt_namabarang().getText());
            b.setHarga_barang(Double.valueOf(frame.getTxt_hargabarang().getText()));
            
            implBarang.update(b);
            
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    //fungsi umtuk menghapus data
    public void hapus()
    {
        int n = JOptionPane.showConfirmDialog(
            null,
            "Apakah Anda yakin ingin menghapus data ini ?",
            "Konfirmasi",
            JOptionPane.YES_NO_OPTION);
        
        if(n == JOptionPane.YES_OPTION)
        {
        implBarang.delete(frame.getTxt_kodebarang().getText());
        JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    public void isiTableCariData() 
    {
        lb = implBarang.getCariData(frame.getTxt_caridata().getText());
        tableModelBarang tmb = new tableModelBarang(lb);
        frame.getT_barang().setModel(tmb);
    }
    
    public void caridata()
    {
        if (frame.getTxt_caridata().getText().trim().isEmpty())
        {
            isiTable();
        }
        else
        {
            implBarang.getCariData(frame.getTxt_caridata().getText());
            isiTableCariData();
        }    
    }
    
}
