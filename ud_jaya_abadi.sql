-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2015 at 03:18 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ud_jaya_abadi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_barang`
--

CREATE TABLE IF NOT EXISTS `t_barang` (
  `kode_barang` varchar(6) NOT NULL,
  `kode_model` varchar(5) NOT NULL,
  `kode_kayu` varchar(5) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga_barang` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_barang`
--

INSERT INTO `t_barang` (`kode_barang`, `kode_model`, `kode_kayu`, `nama_barang`, `harga_barang`) VALUES
('KP001', 'KM001', 'KK001', 'Pintu Ukir Jepara', 100000),
('KP002', 'KM001', 'KK001', 'ewrwer', 100000),
('KP004', 'KM002', 'KK002', 'Kusen Karikatur', 4000000);

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_gaji`
--

CREATE TABLE IF NOT EXISTS `t_detail_gaji` (
  `id_karyawan` int(20) NOT NULL,
  `kode_pekerjaan` int(20) NOT NULL,
  `tanggal_selesai` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_transaksi`
--

CREATE TABLE IF NOT EXISTS `t_detail_transaksi` (
  `no_transaksi` varchar(20) NOT NULL,
  `kode_barang` varchar(6) NOT NULL,
  `ukuran` varchar(8) NOT NULL,
  `harga_satuan` double NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_detail_transaksi`
--

INSERT INTO `t_detail_transaksi` (`no_transaksi`, `kode_barang`, `ukuran`, `harga_satuan`, `qty`, `jumlah`) VALUES
('11202015091455', 'KP001', '0x0', 200000, 2, 400000),
('11202015091455', 'KP004', '2x5', 400000, 1, 400000),
('11202015092729', 'KP001', '0x0', 500000, 1, 500000),
('11202015092729', 'KP004', '3x2', 500000, 1, 500000),
('11202015093213', 'KP001', '0x0', 0, 0, 0),
('11252015185502', 'KP001', '0x0', 100000, 2, 200000),
('11252015185502', 'KP002', '0x0', 100000, 2, 200000),
('11252015190553', 'KP001', '0x0', 300000, 1, 300000),
('11252015190553', 'KP004', '3x2', 500000, 1, 500000),
('11252015190823', 'KP002', '0x0', 200000, 1, 200000),
('11252015190823', 'KP004', '0x0', 200000, 2, 400000),
('11252015191803', 'KP002', '0x0', 200000, 1, 200000),
('11252015191803', 'KP004', '0x0', 200000, 3, 600000),
('11252015193845', 'KP001', '0x0', 300000, 1, 300000),
('11252015193845', 'KP004', '2x2', 400000, 2, 800000),
('11252015194100', 'KP001', '0x0', 500000, 2, 1000000),
('11252015194100', 'KP002', '0x0', 500000, 2, 1000000),
('11252015194318', 'KP002', '0x0', 100000, 2, 200000),
('11252015194446', 'KP002', '0x0', 300000, 2, 600000),
('11252015194630', 'KP004', '3x2', 500000, 2, 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `t_gaji`
--

CREATE TABLE IF NOT EXISTS `t_gaji` (
  `no_gaji` int(20) NOT NULL,
  `id_karyawan` int(20) NOT NULL,
  `jumlah_gaji` int(20) NOT NULL,
  `tanggal` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_karyawan`
--

CREATE TABLE IF NOT EXISTS `t_karyawan` (
  `id_karyawan` varchar(20) NOT NULL,
  `nama_karyawan` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `status` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_karyawan`
--

INSERT INTO `t_karyawan` (`id_karyawan`, `nama_karyawan`, `alamat`, `no_hp`, `username`, `password`, `status`) VALUES
('10113333', 'Ujang Jeprut', 'Bandung', '081573777945', 'admin', 'admin', 'admin'),
('10113334', 'Sueb', 'Cicaheum', '08170216057', 'sueb', 'suebkece', 'pegawai');

-- --------------------------------------------------------

--
-- Table structure for table `t_model`
--

CREATE TABLE IF NOT EXISTS `t_model` (
  `kode_model` varchar(5) NOT NULL DEFAULT '',
  `nama_model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_model`
--

INSERT INTO `t_model` (`kode_model`, `nama_model`) VALUES
('KM001', 'Pintu'),
('KM002', 'Kusen');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_kayu`
--

CREATE TABLE IF NOT EXISTS `t_tipe_kayu` (
  `kode_kayu` varchar(5) NOT NULL,
  `jenis_kayu` varchar(20) NOT NULL,
  `harga` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_kayu`
--

INSERT INTO `t_tipe_kayu` (`kode_kayu`, `jenis_kayu`, `harga`) VALUES
('KK001', 'Jati', 50000),
('KK002', 'Samarinda', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `t_transaksi`
--

CREATE TABLE IF NOT EXISTS `t_transaksi` (
  `no_transaksi` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telpon` varchar(12) NOT NULL,
  `tanggal` date NOT NULL,
  `total_harga` double NOT NULL,
  `bayar` double NOT NULL,
  `sisa` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_transaksi`
--

INSERT INTO `t_transaksi` (`no_transaksi`, `nama_pelanggan`, `alamat`, `no_telpon`, `tanggal`, `total_harga`, `bayar`, `sisa`) VALUES
('11202015091455', 'Ujang Jeprut', 'Jl. cisangkuy', '081573777945', '2015-11-20', 800000, 800000, 0),
('11202015092729', 'Sueb', 'Bandung', '08170216057', '2015-11-20', 1000000, 1000000, 0),
('11202015093213', 'sdsa', 'dsadas', '423424324', '2015-11-20', 0, 0, 0),
('11222015195129', '', '', '', '2015-11-22', 0, 0, 0),
('11252015185502', 'Sueb', 'sdsad', '123123123', '2015-11-25', 0, 0, 0),
('11252015190553', 'asd', 'qweqw', '13123213', '2015-11-25', 300000, 0, 300000),
('11252015190823', 'asd', 'wwqewqe', '12321313', '2015-11-25', 600000, 0, 600000),
('11252015191803', 'asdasd', 'asdsada', '678678678', '2015-11-25', 800000, 200000, 600000),
('11252015193845', 'sdfsdf', 'sdfdsf', '12321312', '2015-11-25', 1100000, 0, 1100000),
('11252015194100', 'jklkj', 'jkljkljklkjl', '456456546', '2015-11-25', 5000000, 2500000, 2500000),
('11252015194318', 'asdasd', 'asdsa', '123123213', '2015-11-25', 200000, 200000, 0),
('11252015194446', 'sadasd', 'dasd', '123123', '2015-11-25', 600000, 300000, 300000),
('11252015194630', 'qweqwewqe', 'sdadsadsa', '213123123123', '2015-11-25', 1000000, 1000000, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_barang`
--
CREATE TABLE IF NOT EXISTS `view_barang` (
`kode_barang` varchar(6)
,`kode_model` varchar(5)
,`nama_model` varchar(100)
,`kode_kayu` varchar(5)
,`jenis_kayu` varchar(20)
,`harga` double
,`nama_barang` varchar(50)
,`harga_barang` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_detail_transaksi`
--
CREATE TABLE IF NOT EXISTS `view_detail_transaksi` (
`no_transaksi` varchar(20)
,`nama_barang` varchar(50)
,`ukuran` varchar(8)
,`harga_satuan` double
,`qty` int(11)
,`jumlah` double
);

-- --------------------------------------------------------

--
-- Structure for view `view_barang`
--
DROP TABLE IF EXISTS `view_barang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_barang` AS (select `tb`.`kode_barang` AS `kode_barang`,`tb`.`kode_model` AS `kode_model`,`tm`.`nama_model` AS `nama_model`,`tb`.`kode_kayu` AS `kode_kayu`,`ttk`.`jenis_kayu` AS `jenis_kayu`,`ttk`.`harga` AS `harga`,`tb`.`nama_barang` AS `nama_barang`,`tb`.`harga_barang` AS `harga_barang` from ((`t_barang` `tb` join `t_model` `tm` on((`tb`.`kode_model` = `tm`.`kode_model`))) join `t_tipe_kayu` `ttk` on((`tb`.`kode_kayu` = `ttk`.`kode_kayu`))));

-- --------------------------------------------------------

--
-- Structure for view `view_detail_transaksi`
--
DROP TABLE IF EXISTS `view_detail_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_detail_transaksi` AS (select `tdt`.`no_transaksi` AS `no_transaksi`,`tb`.`nama_barang` AS `nama_barang`,`tdt`.`ukuran` AS `ukuran`,`tdt`.`harga_satuan` AS `harga_satuan`,`tdt`.`qty` AS `qty`,`tdt`.`jumlah` AS `jumlah` from (`t_detail_transaksi` `tdt` join `t_barang` `tb` on((`tdt`.`kode_barang` = `tb`.`kode_barang`))));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_barang`
--
ALTER TABLE `t_barang`
  ADD PRIMARY KEY (`kode_barang`), ADD KEY `t_barang_ibfk_1` (`kode_model`), ADD KEY `t_barang_ibfk_2` (`kode_kayu`);

--
-- Indexes for table `t_detail_transaksi`
--
ALTER TABLE `t_detail_transaksi`
  ADD PRIMARY KEY (`no_transaksi`,`kode_barang`), ADD KEY `kode_barang` (`kode_barang`);

--
-- Indexes for table `t_gaji`
--
ALTER TABLE `t_gaji`
  ADD PRIMARY KEY (`no_gaji`);

--
-- Indexes for table `t_karyawan`
--
ALTER TABLE `t_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `t_model`
--
ALTER TABLE `t_model`
  ADD PRIMARY KEY (`kode_model`);

--
-- Indexes for table `t_tipe_kayu`
--
ALTER TABLE `t_tipe_kayu`
  ADD PRIMARY KEY (`kode_kayu`);

--
-- Indexes for table `t_transaksi`
--
ALTER TABLE `t_transaksi`
  ADD PRIMARY KEY (`no_transaksi`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_barang`
--
ALTER TABLE `t_barang`
ADD CONSTRAINT `t_barang_ibfk_1` FOREIGN KEY (`kode_model`) REFERENCES `t_model` (`kode_model`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_barang_ibfk_2` FOREIGN KEY (`kode_kayu`) REFERENCES `t_tipe_kayu` (`kode_kayu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_detail_transaksi`
--
ALTER TABLE `t_detail_transaksi`
ADD CONSTRAINT `t_detail_transaksi_ibfk_1` FOREIGN KEY (`no_transaksi`) REFERENCES `t_transaksi` (`no_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_detail_transaksi_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `t_barang` (`kode_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
