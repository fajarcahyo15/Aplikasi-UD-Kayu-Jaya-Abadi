/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hikira1
 */
public class tableModelModel extends AbstractTableModel {
    List<model> lb;

    public tableModelModel(List<model> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Kode Model";
            case 1:
                return "Nama Model";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getKode_model();
            case 1:
                return lb.get(row).getNama_model();
            
            default:
                return null;
        }
    }
}
