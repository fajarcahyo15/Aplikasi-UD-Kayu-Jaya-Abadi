/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAOImplement;

import java.util.List;
import penjulan_penggajian_jaya_abadi.model.*;

/**
 *
 * @author Ujang Jeprut
 */
public interface ImplementDetailtransaksi {
    public void insert(detail_transaksi b);
    
    public void delete(String no_transaksi, String kode_barang);
    
    public List<detail_transaksi> getALL();
    
    public List<detail_transaksi> getCariData(String data);
    
}
