/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.controller;

import java.util.List;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAO.daoKaryawan;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementKaryawan;
import penjulan_penggajian_jaya_abadi.model.karyawan;
import penjulan_penggajian_jaya_abadi.model.tableModelKaryawan;
import penjulan_penggajian_jaya_abadi.model.teksModel;
import penjulan_penggajian_jaya_abadi.view.frm_karyawan;

/**
 *
 * @author hikira1
 */
public class controllerKaryawan {
    frm_karyawan frame;

    ImplementKaryawan implKaryawan;
    List<karyawan> lb;
    
    public controllerKaryawan(frm_karyawan frame)
    {
        this.frame = frame;
        implKaryawan = new daoKaryawan();
        lb = implKaryawan.getALL();
    }
    
     public void konfig_teks()
    {
        teksModel teks_1 = new teksModel(8);
        teksModel teks_2 = new teksModel(20);
        teksModel teks_3 = new teksModel(12);
        frame.getTxt_idkaryawan().setDocument(teks_1);
        frame.getTxt_namakarywan().setDocument(teks_2);
        frame.getTxt_hpkaryawan().setDocument(teks_3);

    }
    public void reset()
    {
        frame.getTxt_idkaryawan().setText("");
        frame.getTxt_namakarywan().setText("");
        frame.getTxt_hpkaryawan().setText("");
        frame.getTxt_alamatkaryawan().setText("");
        frame.getTxt_caridata().setText("");
        
        frame.getTxt_idkaryawan().setEnabled(false);
        frame.getTxt_namakarywan().setEnabled(false);
        frame.getTxt_hpkaryawan().setEnabled(false);
        frame.getTxt_alamatkaryawan().setEnabled(false);
        frame.getTxt_caridata().setEnabled(true);
       
        
        frame.getBtn_tambah().setEnabled(true);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(false);
        frame.getBtn_keluar().setEnabled(true);
    }
    
    public void tambah()
    {
        frame.getTxt_idkaryawan().setEnabled(true);
        frame.getTxt_namakarywan().setEnabled(true);
        frame.getTxt_hpkaryawan().setEnabled(true);
        frame.getTxt_alamatkaryawan().setEnabled(true);
        frame.getTxt_caridata().setText("");
        
        frame.getTxt_caridata().setEnabled(false);
        
        frame.getTxt_idkaryawan().requestFocus();
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
     public void tabel_click()
    {
        frame.getTxt_idkaryawan().setEnabled(false);
        frame.getTxt_namakarywan().setEnabled(true);
        frame.getTxt_hpkaryawan().setEnabled(true);
        frame.getTxt_alamatkaryawan().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(true);
        frame.getBtn_hapus().setEnabled(true);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
      public void isiTable()
    {
        lb = implKaryawan.getALL();
        tableModelKaryawan tmb = new tableModelKaryawan(lb);
        frame.getT_karywan().setModel(tmb);
    }
    
    public void isiField(int row)
    {
        frame.getTxt_idkaryawan().setText(lb.get(row).getId_karyawan().toString());
        frame.getTxt_namakarywan().setText(lb.get(row).getNama_karyawan().toString());
        frame.getTxt_hpkaryawan().setText(lb.get(row).getNo_hp().toString());
        frame.getTxt_alamatkaryawan().setText(lb.get(row).getAlamat().toString());

    }
    public void simpan()
    {
        if((!frame.getTxt_idkaryawan().getText().trim().isEmpty()) && 
                (!frame.getTxt_namakarywan().getText().trim().isEmpty()) &&
                (!frame.getTxt_hpkaryawan().getText().trim().isEmpty()) &&
                (!frame.getTxt_alamatkaryawan().getText().trim().isEmpty()))
        {
            karyawan b = new karyawan();
            b.setId_karyawan(frame.getTxt_idkaryawan().getText());
            b.setNama_karyawan(frame.getTxt_namakarywan().getText());
            b.setNo_hp(frame.getTxt_hpkaryawan().getText());
            b.setAlamat(frame.getTxt_alamatkaryawan().getText());
            
            implKaryawan.insert(b);
            
            
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
     public void ubah() 
    {
        if((!frame.getTxt_idkaryawan().getText().trim().isEmpty()))
        {
            karyawan b = new karyawan();
            b.setId_karyawan(frame.getTxt_idkaryawan().getText());
            b.setNama_karyawan(frame.getTxt_namakarywan().getText());
            b.setNo_hp(frame.getTxt_hpkaryawan().getText());
            b.setAlamat(frame.getTxt_alamatkaryawan().getText());
            
            implKaryawan.update(b);
            
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
      public void hapus()
    {
        int n = JOptionPane.showConfirmDialog(
            null,
            "Apakah Anda yakin ingin menghapus data ini ?",
            "Konfirmasi",
            JOptionPane.YES_NO_OPTION);
        
        if(n == JOptionPane.YES_OPTION)
        {
        implKaryawan.delete(frame.getTxt_idkaryawan().getText());
        JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    public void isiTableCariData() 
    {
        lb = implKaryawan.getCariData(frame.getTxt_caridata().getText());
        tableModelKaryawan tmb = new tableModelKaryawan(lb);
        frame.getT_karywan().setModel(tmb);
    }
    
    public void caridata()
    {
        if (frame.getTxt_caridata().getText().trim().isEmpty())
        {
            isiTable();
        }
        else
        {
            implKaryawan.getCariData(frame.getTxt_caridata().getText());
            isiTableCariData();
        }    
    }
    
     
}
