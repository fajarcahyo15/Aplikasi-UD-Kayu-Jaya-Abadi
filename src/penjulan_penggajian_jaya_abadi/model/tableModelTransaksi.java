/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ujang Jeprut
 */
public class tableModelTransaksi extends AbstractTableModel {
    List<transaksi> lb;

    public tableModelTransaksi(List<transaksi> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }


    @Override
    public int getRowCount() {
        return lb.size();
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nomor Transaksi";
            case 1:
                return "Nama Pelanggan";
            case 2:
                return "Alamat";
            case 3:
                return "No. Telp";
            case 4:
                return "Tanggal";
            case 5:
                return "Total Harga";
            case 6:
                return "Uang Muka";
            case 7:
                return "Sisa";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getNo_transaksi();
            case 1:
                return lb.get(row).getNama_pelanggan();
            case 2:
                return lb.get(row).getAlamat();
            case 3:
                return lb.get(row).getNo_telp();
            case 4:
                return lb.get(row).getTanggal();
            case 5:
                return lb.get(row).getTotal_harga();
            case 6:
                return lb.get(row).getBayar();
            case 7:
                return lb.get(row).getSisa();    
            default:
                return null;
        }
    }
}
