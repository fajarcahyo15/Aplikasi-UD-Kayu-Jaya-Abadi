/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Ujang Jeprut
 */
public class teksModel extends PlainDocument {
    private int max;
    
    public teksModel(int max) {
        this.max = max;
    }
    
    public teksModel() {
        this(-1);
    }
    
    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        if(this.max == -1){
            super.insertString(offs, str, a);
        }else{
            int panjangYangMasuk = str.length();
            int panjangTeks = getLength();
            if((panjangTeks + panjangYangMasuk) > this.max){
                // teks tidak dimasukkan
            }else{
                super.insertString(offs, str, a);
            }
        }
    }
}
