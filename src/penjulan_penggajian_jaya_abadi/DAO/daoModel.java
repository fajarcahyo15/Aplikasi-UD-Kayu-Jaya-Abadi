/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.koneksi.koneksi;
import penjulan_penggajian_jaya_abadi.model.model;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementModel;

/**
 *
 * @author hikira1
 */
public class daoModel implements ImplementModel {
    Connection connection;
    final String insert="INSERT INTO t_model (kode_model,nama_model) VALUES (?,?);";
    final String update = "UPDATE t_model set nama_model=? where kode_model=? ;";
    final String delete = "DELETE FROM t_model where kode_model=? ;";
    final String select = "SELECT * FROM t_model;";
    final String caridata = "SELECT * FROM t_model where kode_model like ? or nama_model like ?";
    
    public daoModel() {
        connection = koneksi.connection();
    }
    
    public void insert(model b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getKode_model());
            statement.setString(2, b.getNama_model());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            //ResultSet rs = statement.getGeneratedKeys();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Kode Model sudah ada, Gagal menyimpan !",
                    "Information",JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void update(model b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getNama_model());
            statement.setString(2, b.getKode_model());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(String kode_model) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setString(1, kode_model);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<model> getALL() {
        List<model> lb = null;
        try {
            lb = new ArrayList<model>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                model b = new model();
                b.setKode_model(rs.getString("kode_model"));
                b.setNama_model(rs.getString("nama_model"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<model> getCariData(String data) {
        List<model> lb = null;
        try {
            lb = new ArrayList<model>();
            PreparedStatement st = connection.prepareStatement(caridata);
            st.setString(1, "%" + data + "%");
            st.setString(2, "%" + data + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                model b = new model();
                b.setKode_model(rs.getString("kode_model"));
                b.setNama_model(rs.getString("nama_model"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
}
