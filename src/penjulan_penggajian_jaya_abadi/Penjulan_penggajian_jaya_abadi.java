/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi;

import com.jtattoo.plaf.texture.TextureLookAndFeel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import penjulan_penggajian_jaya_abadi.view.frm_barang;
import penjulan_penggajian_jaya_abadi.view.frm_daftar;
import penjulan_penggajian_jaya_abadi.view.frm_karyawan;
import penjulan_penggajian_jaya_abadi.view.frm_login;
import penjulan_penggajian_jaya_abadi.view.frm_model;
import penjulan_penggajian_jaya_abadi.view.frm_penjualan;
import penjulan_penggajian_jaya_abadi.view.frm_tipekayu;
import penjulan_penggajian_jaya_abadi.view.frm_utama;



/**
 *
 * @author Ujang Jeprut
 */
public class Penjulan_penggajian_jaya_abadi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try { 
            UIManager.setLookAndFeel(new TextureLookAndFeel()); 
            // TODO code application logic here
            //new frm_tipekayu().setVisible(true);
            //new frm_model().setVisible(true);
            //new frm_barang().setVisible(true);
            //new frm_karyawan().setVisible(true);
            //new frm_utama().setVisible(true);
            //new frm_penjualan().setVisible(true);
            new frm_login().setVisible(true);
            //new frm_daftar().setVisible(true);
        } catch (UnsupportedLookAndFeelException ex) { 
            JOptionPane.showMessageDialog(null,"Sistem Operasi Anda Tidak Mendukung Tampilan Aplikasi ini, "
                    + "Tampilan Akan Berubah Menjadi Default..","Perhatian",JOptionPane.ERROR_MESSAGE);
        }           
    }
    
}
