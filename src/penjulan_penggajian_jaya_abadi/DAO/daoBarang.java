/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementBarang;
import penjulan_penggajian_jaya_abadi.koneksi.koneksi;
import penjulan_penggajian_jaya_abadi.model.barang;

/**
 *
 * @author Ujang Jeprut
 */
public class daoBarang implements ImplementBarang {
    Connection connection;
    final String insert="INSERT INTO t_barang (kode_barang,kode_model,kode_kayu,nama_barang,harga_barang) VALUES (?,?,?,?,?);";
    final String update = "UPDATE t_barang set kode_model=?, kode_kayu=?, nama_barang=?, harga_barang=? where kode_barang=? ;";
    final String delete = "DELETE FROM t_barang where kode_barang=? ;";
    final String select = "SELECT * FROM view_barang;";
    final String caridata = "SELECT * FROM view_barang where kode_barang like ? or kode_model like ? or kode_kayu like ? or nama_barang like ?";
    
    public daoBarang() {
        connection = koneksi.connection();
    }
     
    public void insert(barang b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getKode_barang());
            statement.setString(2, b.getKode_model());
            statement.setString(3, b.getKode_kayu());
            statement.setString(4, b.getNama_barang());
            statement.setDouble(5, b.getHarga_barang());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            //ResultSet rs = statement.getGeneratedKeys();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Kode Barang sudah ada, Gagal menyimpan !",
                    "Information",JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void update(barang b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getKode_model());
            statement.setString(2, b.getKode_kayu());
            statement.setString(3, b.getNama_barang());
            statement.setDouble(4, b.getHarga_barang());
            statement.setString(5, b.getKode_barang());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(String kode_barang) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setString(1, kode_barang);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<barang> getALL() {
        List<barang> lb = null;
        try {
            lb = new ArrayList<barang>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                barang b = new barang();
                b.setKode_barang(rs.getString("kode_barang"));
                b.setKode_model(rs.getString("kode_model"));
                b.setNama_model(rs.getString("nama_model"));
                b.setKode_kayu(rs.getString("kode_kayu"));
                b.setJenis_kayu(rs.getString("jenis_kayu"));
                b.setHarga_kayu(rs.getDouble("harga"));
                b.setNama_barang(rs.getString("nama_barang"));
                b.setHarga_barang(rs.getDouble("harga_barang"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoBarang.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<barang> getCariData(String data) {
        List<barang> lb = null;
        try {
            lb = new ArrayList<barang>();
            PreparedStatement st = connection.prepareStatement(caridata);
            st.setString(1, "%" + data + "%");
            st.setString(2, "%" + data + "%");
            st.setString(3, "%" + data + "%");
            st.setString(4, "%" + data + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                barang b = new barang();
                b.setKode_barang(rs.getString("kode_barang"));
                b.setKode_model(rs.getString("kode_model"));
                b.setNama_model(rs.getString("nama_model"));
                b.setKode_kayu(rs.getString("kode_kayu"));
                b.setJenis_kayu(rs.getString("jenis_kayu"));
                b.setHarga_kayu(rs.getDouble("harga"));
                b.setNama_barang(rs.getString("nama_barang"));
                b.setHarga_barang(rs.getDouble("harga_barang"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
}
