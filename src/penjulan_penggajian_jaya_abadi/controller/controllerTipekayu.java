/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.controller;

import java.util.List;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAO.daoTipekayu;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementTipekayu;
import penjulan_penggajian_jaya_abadi.model.tableModelTipekayu;
import penjulan_penggajian_jaya_abadi.model.teksModel;
import penjulan_penggajian_jaya_abadi.model.tipekayu;
import penjulan_penggajian_jaya_abadi.view.frm_tipekayu;

/**
 *
 * @author Ujang Jeprut
 */
public class controllerTipekayu {
    frm_tipekayu frame;
    ImplementTipekayu implTipekayu;
    List<tipekayu> lb;
    
    public controllerTipekayu(frm_tipekayu frame)
    {
        this.frame = frame;
        implTipekayu = new daoTipekayu();
        lb = implTipekayu.getALL();
    }
    
    //membuat kondisi default program saat terjadi aksi tertentu
    public void konfig_teks()
    {
        teksModel teks_kd = new teksModel(5);
        teksModel teks_jenis = new teksModel(20);
        frame.getTxt_kodekayu().setDocument(teks_kd);
        frame.getTxt_jeniskayu().setDocument(teks_jenis);
    }
    
    public void reset()
    {
        frame.getTxt_kodekayu().setText("");
        frame.getTxt_jeniskayu().setText("");
        frame.getTxt_hargakayu().setText("");
        frame.getTxt_caridata().setText("");
        
        frame.getTxt_kodekayu().setEnabled(false);
        frame.getTxt_jeniskayu().setEnabled(false);
        frame.getTxt_hargakayu().setEnabled(false);
        frame.getTxt_caridata().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(true);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(false);
        frame.getBtn_keluar().setEnabled(true);
    }
    
    //fungsi untuk tombol tambah
    public void tambah()
    {
        frame.getTxt_kodekayu().setEnabled(true);
        frame.getTxt_jeniskayu().setEnabled(true);
        frame.getTxt_hargakayu().setEnabled(true);
        frame.getTxt_caridata().setText("");
        frame.getTxt_caridata().setEnabled(false);
        
        frame.getTxt_kodekayu().requestFocus();
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
    public void tabel_click()
    {
        frame.getTxt_kodekayu().setEnabled(false);
        frame.getTxt_jeniskayu().setEnabled(true);
        frame.getTxt_hargakayu().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(true);
        frame.getBtn_hapus().setEnabled(true);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
    //menampilkan isi data ke dalam tabel
    public void isiTable()
    {
        lb = implTipekayu.getALL();
        tableModelTipekayu tmb = new tableModelTipekayu(lb);
        frame.getT_jeniskayu().setModel(tmb);
    }
    
    public void isiField(int row)
    {
        frame.getTxt_kodekayu().setText(lb.get(row).getKode_kayu().toString());
        frame.getTxt_jeniskayu().setText(lb.get(row).getJenis_kayu().toString());
        frame.getTxt_hargakayu().setText(lb.get(row).getHarga().toString());
    }
    
    //fungsi untuk menyimpan data
    public void simpan()
    {
        if((!frame.getTxt_kodekayu().getText().trim().isEmpty()) && 
                (!frame.getTxt_jeniskayu().getText().trim().isEmpty()) && 
                (!frame.getTxt_hargakayu().getText().trim().isEmpty()))
        {
            tipekayu b = new tipekayu();
            b.setKode_kayu(frame.getTxt_kodekayu().getText());
            b.setJenis_kayu(frame.getTxt_jeniskayu().getText());
            b.setHarga(Double.valueOf(frame.getTxt_hargakayu().getText()));
            
            implTipekayu.insert(b);
            
            
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    //fungsi untuk mengubah data
    public void ubah() 
    {
        if((!frame.getTxt_kodekayu().getText().trim().isEmpty()))
        {
            tipekayu b = new tipekayu();
            b.setKode_kayu(frame.getTxt_kodekayu().getText());
            b.setJenis_kayu(frame.getTxt_jeniskayu().getText());
            b.setHarga(Double.valueOf(frame.getTxt_hargakayu().getText()));
            
            implTipekayu.update(b);
            
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    //fungsi umtuk menghapus data
    public void hapus()
    {
        int n = JOptionPane.showConfirmDialog(
            null,
            "Apakah Anda yakin ingin menghapus data ini ?",
            "Konfirmasi",
            JOptionPane.YES_NO_OPTION);
        
        if(n == JOptionPane.YES_OPTION)
        {
        implTipekayu.delete(frame.getTxt_kodekayu().getText());
        JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    public void isiTableCariData() 
    {
        lb = implTipekayu.getCariData(frame.getTxt_caridata().getText());
        tableModelTipekayu tmb = new tableModelTipekayu(lb);
        frame.getT_jeniskayu().setModel(tmb);
    }
    
    public void caridata()
    {
        if (frame.getTxt_caridata().getText().trim().isEmpty())
        {
            isiTable();
        }
        else
        {
            implTipekayu.getCariData(frame.getTxt_caridata().getText());
            isiTableCariData();
        }
        
    }
   
}
