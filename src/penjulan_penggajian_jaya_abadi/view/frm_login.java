/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAO.daoKaryawan;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementKaryawan;
import penjulan_penggajian_jaya_abadi.model.karyawan;

/**
 *
 * @author GigaByte
 */
public class frm_login extends javax.swing.JFrame {

    /**
     * Creates new form frm_login
     */
    ImplementKaryawan implKaryawan;
    List<karyawan> lb = new ArrayList<karyawan>();
    Integer valid = 0;
    
    public frm_login() {
        initComponents();
        implKaryawan = new daoKaryawan();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_username = new javax.swing.JTextField();
        btn_keluar = new javax.swing.JButton();
        btn_login = new javax.swing.JButton();
        txt_password = new javax.swing.JPasswordField();
        bg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login Aplikasi UD Jaya Abadi");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(txt_username, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 160, -1));

        btn_keluar.setText("Keluar");
        getContentPane().add(btn_keluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 150, 70, -1));

        btn_login.setText("Login");
        btn_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_loginActionPerformed(evt);
            }
        });
        getContentPane().add(btn_login, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 150, 70, -1));

        txt_password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_passwordActionPerformed(evt);
            }
        });
        getContentPane().add(txt_password, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 160, -1));

        bg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/penjulan_penggajian_jaya_abadi/view/image/loginBG.png"))); // NOI18N
        getContentPane().add(bg, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 320, 220));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_loginActionPerformed
        // TODO add your handling code here:
        try{
           lb = implKaryawan.getLogin(txt_username.getText(), (txt_password.getText()));
           if((lb.size()>=1)&&(valid<=3)) {
                new frm_utama().setVisible(true);
                this.setVisible(false);
           }
           else if(valid <3){
               JOptionPane.showMessageDialog(null, "Username atau Password Salah !","Informasi",
               JOptionPane.WARNING_MESSAGE);
               txt_username.requestFocus();
               valid++;   
           }
           else if(valid>=3)
           {
               JOptionPane.showMessageDialog(null, "Gagal Login Lebih dari 3 kali, Keluar !","Informasi",
               JOptionPane.WARNING_MESSAGE);
               System.exit(0);
           }
        }     
        catch(Exception a){
                JOptionPane.showMessageDialog(null, a);   
        }  
                                      
    }//GEN-LAST:event_btn_loginActionPerformed

    private void txt_passwordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_passwordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_passwordActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frm_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frm_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frm_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frm_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frm_login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bg;
    private javax.swing.JButton btn_keluar;
    private javax.swing.JButton btn_login;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JTextField txt_username;
    // End of variables declaration//GEN-END:variables
}
