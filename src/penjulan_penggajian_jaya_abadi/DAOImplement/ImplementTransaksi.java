/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAOImplement;

import java.util.List;
import penjulan_penggajian_jaya_abadi.model.*;

/**
 *
 * @author Ujang Jeprut
 */
public interface ImplementTransaksi {
    public void insert(transaksi b);
    
    public void update(transaksi b);

    public void delete(String no_transaksi);

    public List<transaksi> getALL();

    public List<transaksi> getCariData(String data);
}
