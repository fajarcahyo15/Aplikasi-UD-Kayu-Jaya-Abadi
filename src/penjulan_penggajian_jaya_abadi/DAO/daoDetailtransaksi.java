/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementDetailtransaksi;
import penjulan_penggajian_jaya_abadi.koneksi.koneksi;
import penjulan_penggajian_jaya_abadi.model.detail_transaksi;
import penjulan_penggajian_jaya_abadi.model.transaksi;

/**
 *
 * @author Ujang Jeprut
 */
public class daoDetailtransaksi implements ImplementDetailtransaksi{
    Connection connection;
    final String insert="INSERT INTO t_detail_transaksi (no_transaksi,kode_barang,ukuran,harga_satuan,qty,jumlah) VALUES (?,?,?,?,?,?);";
    final String delete = "DELETE FROM t_detail_transaksi where no_transaksi=? and kode_barang=?;";
    final String select = "SELECT * FROM view_detail_transaksi";
    final String caridata = "SELECT nama_barang,ukuran,harga_satuan,qty,jumlah FROM view_detail_transaksi where no_transaksi LIKE ?";
    final String totalbayar = "select sum(jumlah) from t_detail_transaksi where no_transaksi=? group by no_transaksi";
    
    public daoDetailtransaksi() {
        connection = koneksi.connection();
    }
    
    public void insert(detail_transaksi b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getNo_transaksi());
            statement.setString(2, b.getKode_barang());
            statement.setString(3, b.getUkuran());
            statement.setDouble(4, b.getHarga_satuan());
            statement.setInt(5, b.getQty());
            statement.setDouble(6, b.getJumlah());

            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            //ResultSet rs = statement.getGeneratedKeys();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan !\n Tidak diperbolehkan membeli barang yang sama",
                    "Information",JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(String no_transaksi, String kode_barang) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setString(1, no_transaksi);
            statement.setString(2, kode_barang);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<detail_transaksi> getALL() {
        List<detail_transaksi> lb = null;
        try {
            lb = new ArrayList<detail_transaksi>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                detail_transaksi b = new detail_transaksi();
                //b.setNo_transaksi(rs.getString("no_transaksi"));
                b.setNama_barang(rs.getString("nama_barang"));
                b.setUkuran(rs.getString("ukuran"));
                b.setHarga_satuan(rs.getDouble("harga_satuan"));
                b.setQty(rs.getInt("qty"));
                b.setJumlah(rs.getDouble("jumlah"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<detail_transaksi> getCariData(String data) {
        List<detail_transaksi> lb = null;
        try {
            lb = new ArrayList<detail_transaksi>();
            PreparedStatement st = connection.prepareStatement(caridata);
            st.setString(1, "%" + data + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                detail_transaksi b = new detail_transaksi();
                //b.setNo_transaksi(rs.getString("no_transaksi"));
                b.setNama_barang(rs.getString("nama_barang"));
                b.setUkuran(rs.getString("ukuran"));
                b.setHarga_satuan(rs.getDouble("harga_satuan"));
                b.setQty(rs.getInt("qty"));
                b.setJumlah(rs.getDouble("jumlah"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoDetailtransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
}
