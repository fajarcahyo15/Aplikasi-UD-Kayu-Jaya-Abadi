/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.controller;

import java.util.List;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAO.daoKaryawan;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementKaryawan;
import penjulan_penggajian_jaya_abadi.model.karyawan;
import penjulan_penggajian_jaya_abadi.view.frm_daftar;

/**
 *
 * @author Ujang Jeprut
 */
public class controllerDaftar {
    frm_daftar frame;

    ImplementKaryawan implKaryawan;
    List<karyawan> lb;
    
    public controllerDaftar(frm_daftar frame)
    {
        this.frame = frame;
        implKaryawan = new daoKaryawan();
        lb = implKaryawan.getALL();
    }
    
    public void awal()
    {
        frame.getTxt_id().setEditable(false);
        frame.getTxt_username().setText("");
        frame.getTxt_password().setText("");
        frame.getTxt_konfirmasi().setText("");
        
        
        frame.getBtn_daftar().setEnabled(true);
        frame.getBtn_keluar().setEnabled(true);
    }
    
    public void daftar()
    {
        if((!frame.getTxt_id().getText().trim().isEmpty()) &&
                (!frame.getTxt_username().getText().trim().isEmpty()) && 
                (!frame.getTxt_password().getText().trim().isEmpty()) &&
                (frame.getTxt_password().getText().trim().equals(frame.getTxt_konfirmasi().getText().trim())))
        {
            karyawan b = new karyawan();
            b.setId_karyawan(frame.getTxt_id().getText().trim());
            b.setUsername(frame.getTxt_username().getText().trim());
            b.setPassword(frame.getTxt_password().getText().trim());
            implKaryawan.daftar(b);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Data Tidak Valid");
        }
    }
}
