<h2>Tugas Sistem Basis Data UNIKOM 2015</h2>

<ul>
    <li>Deskripsi :
        <ul>
            <li>Aplikasi Pengolahan Data Penjualan di UD Kayu Jaya Abadi</li>
        </ul>
    </li>
    <li>Fitur :
        <ul>
            <li>Pengolahan Data Jenis Kayu</li>
            <li>Pengolahan Data Kategori Barang</li>
            <li>Pengolahan Data Penjualan</li>
            <li>Membuat Report Penjualan (Perhari/Perbulan)</li>
            <li>Ekspor/Impor Database</li>
        </ul>
    </li>
    <li>Teknologi :
        <ul>
            <li><a href="https://java.com/en/">Java</a></li>
            <li><a href="https://www.mysql.com/">MySQL</a></li>
        </ul>
    </li>
</ul>