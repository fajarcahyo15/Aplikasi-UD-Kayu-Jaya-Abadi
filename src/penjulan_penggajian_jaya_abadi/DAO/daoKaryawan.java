/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementKaryawan;
import penjulan_penggajian_jaya_abadi.koneksi.koneksi;
import penjulan_penggajian_jaya_abadi.model.karyawan;
/**
 *
 * @author Ujang Jeprut
 */
public class daoKaryawan implements ImplementKaryawan {
    Connection connection;
    final String insert="INSERT INTO t_karyawan (id_karyawan,nama_karyawan,alamat,no_hp) VALUES (?,?,?,?);";
    final String update = "UPDATE t_karyawan set nama_karyawan=?, alamat=?, no_hp=? where id_karyawan=? ;";
    final String delete = "DELETE FROM t_karyawan where id_karyawan=? ;";
    final String select = "SELECT * FROM t_karyawan;";
    final String caridata = "SELECT * FROM t_karyawan where id_karyawan like ? or nama_karyawan like ? or no_hp like ?";
    final String login = "SELECT * FROM t_karyawan where username=? and password=?";
    final String daftar = "update t_karyawan set username=?, password=? where id_karyawan=?";
    
    public daoKaryawan() {
        connection = koneksi.connection();
    }
    
    public void insert(karyawan b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getId_karyawan());
            statement.setString(2, b.getNama_karyawan());
            statement.setString(3, b.getAlamat());
            statement.setString(4, b.getNo_hp());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            //ResultSet rs = statement.getGeneratedKeys();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ID Karyawan sudah ada, Gagal menyimpan !",
                    "Information",JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void update(karyawan b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getNama_karyawan());
            statement.setString(2, b.getAlamat());
            statement.setString(3, b.getNo_hp());
            statement.setString(4, b.getId_karyawan());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(String id_karyawan) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setString(1, id_karyawan);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<karyawan> getALL() {
        List<karyawan> lb = null;
        try {
            lb = new ArrayList<karyawan>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                karyawan b = new karyawan();
                b.setId_karyawan(rs.getString("id_karyawan"));
                b.setNama_karyawan(rs.getString("nama_karyawan"));
                b.setAlamat(rs.getString("alamat"));
                b.setNo_hp(rs.getString("no_hp"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoKaryawan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<karyawan> getCariData(String data) {
        List<karyawan> lb = null;
        try {
            lb = new ArrayList<karyawan>();
            PreparedStatement st = connection.prepareStatement(caridata);
            st.setString(1, "%" + data + "%");
            st.setString(2, "%" + data + "%");
            st.setString(3, "%" + data + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                karyawan b = new karyawan();
                b.setId_karyawan(rs.getString("id_karyawan"));
                b.setNama_karyawan(rs.getString("nama_karyawan"));
                b.setAlamat(rs.getString("alamat"));
                b.setNo_hp(rs.getString("no_hp"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public List<karyawan> getLogin(String username, String password) {
        List<karyawan> lb = new ArrayList<karyawan>();
        try {
            PreparedStatement st = connection.prepareStatement(login);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                karyawan b = new karyawan();
                b.setUsername(rs.getString("username"));
                b.setPassword(rs.getString("password"));
                b.setStatus("status");
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoKaryawan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
    
    public void daftar(karyawan b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(daftar);
            statement.setString(1, b.getUsername());
            statement.setString(2, b.getPassword());
            statement.setString(3, b.getId_karyawan());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Pegawai Berhasil Didaftarkan");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan !\n"+ex,
                    "Information",JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
