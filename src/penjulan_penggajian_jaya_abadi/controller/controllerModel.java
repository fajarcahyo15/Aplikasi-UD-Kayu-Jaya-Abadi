/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.controller;

import java.util.List;
import javax.swing.JOptionPane;
import penjulan_penggajian_jaya_abadi.DAO.daoModel;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementModel;
import penjulan_penggajian_jaya_abadi.model.model;
import penjulan_penggajian_jaya_abadi.model.tableModelModel;
import penjulan_penggajian_jaya_abadi.model.teksModel;
import penjulan_penggajian_jaya_abadi.view.frm_model;

/**
 *
 * @author Ujang Jeprut
 */
public class controllerModel {
    frm_model frame;
    ImplementModel implModel;
    List<model> lb;
    
    public controllerModel(frm_model frame)
    {
        this.frame = frame;
        implModel = new daoModel();
        lb = implModel.getALL();
    }
    
    //membuat kondisi default program saat terjadi aksi tertentu
    public void konfig_teks()
    {
        teksModel teks_kd = new teksModel(5);
        teksModel teks_nama = new teksModel(100);
        frame.getTxt_kodemodel().setDocument(teks_kd);
        frame.getTxt_namamodel().setDocument(teks_nama);
    }
    
    public void reset()
    {
        frame.getTxt_kodemodel().setText("");
        frame.getTxt_namamodel().setText("");
        //frame.getTxt_cari().setText("");
        
        frame.getTxt_kodemodel().setEnabled(false);
        frame.getTxt_namamodel().setEnabled(false);
        frame.getTxt_caridata().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(true);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(false);
        frame.getBtn_keluar().setEnabled(true);
    }
    
    //fungsi untuk tombol tambah
    public void tambah()
    {
        frame.getTxt_kodemodel().setEnabled(true);
        frame.getTxt_namamodel().setEnabled(true);
        frame.getTxt_caridata().setText("");
        frame.getTxt_caridata().setEnabled(false);
        
        frame.getTxt_kodemodel().requestFocus();
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
    public void tabel_click()
    {
        frame.getTxt_kodemodel().setEnabled(false);
        frame.getTxt_namamodel().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(true);
        frame.getBtn_hapus().setEnabled(true);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
    }
    
    //menampilkan isi data ke dalam tabel
    public void isiTable()
    {
        lb = implModel.getALL();
        tableModelModel tmb = new tableModelModel(lb);
        frame.getT_jenismodel().setModel(tmb);
    }
    
    public void isiField(int row)
    {
        frame.getTxt_kodemodel().setText(lb.get(row).getKode_model().toString());
        frame.getTxt_namamodel().setText(lb.get(row).getNama_model().toString());
    }
    
    //fungsi untuk menyimpan data
    public void simpan()
    {
        if((!frame.getTxt_kodemodel().getText().trim().isEmpty()) && 
                (!frame.getTxt_namamodel().getText().trim().isEmpty()))
        {
            model b = new model();
            b.setKode_model(frame.getTxt_kodemodel().getText());
            b.setNama_model(frame.getTxt_namamodel().getText());
            
            implModel.insert(b);
            
            
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    //fungsi untuk mengubah data
    public void ubah() 
    {
        if((!frame.getTxt_kodemodel().getText().trim().isEmpty()))
        {
            model b = new model();
            b.setKode_model(frame.getTxt_kodemodel().getText());
            b.setNama_model(frame.getTxt_namamodel().getText());
            
            implModel.update(b);
            
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
            reset();
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    //fungsi umtuk menghapus data
    public void hapus()
    {
        int n = JOptionPane.showConfirmDialog(
            null,
            "Apakah Anda yakin ingin menghapus data ini ?",
            "Konfirmasi",
            JOptionPane.YES_NO_OPTION);
        
        if(n == JOptionPane.YES_OPTION)
        {
        implModel.delete(frame.getTxt_kodemodel().getText());
        JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
        }
    }
 
    
    public void isiTableCariData() 
    {
        lb = implModel.getCariData(frame.getTxt_caridata().getText());
        tableModelModel tmb = new tableModelModel(lb);
        frame.getT_jenismodel().setModel(tmb);
    }
    
    public void caridata()
    {
        if (frame.getTxt_caridata().getText().trim().isEmpty())
        {
            isiTable();
        }
        else
        {
            implModel.getCariData(frame.getTxt_caridata().getText());
            isiTableCariData();
        }
        
    }
    
    
}
