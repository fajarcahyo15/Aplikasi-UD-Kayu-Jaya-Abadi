/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import penjulan_penggajian_jaya_abadi.DAO.daoDetailtransaksi;
import penjulan_penggajian_jaya_abadi.DAO.daoTransaksi;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementDetailtransaksi;
import penjulan_penggajian_jaya_abadi.DAOImplement.ImplementTransaksi;
import penjulan_penggajian_jaya_abadi.model.detail_transaksi;
import penjulan_penggajian_jaya_abadi.model.tableModelDetailTransaksi;
import penjulan_penggajian_jaya_abadi.model.tableModelTransaksi;
import penjulan_penggajian_jaya_abadi.model.teksModel;
import penjulan_penggajian_jaya_abadi.model.transaksi;
import penjulan_penggajian_jaya_abadi.view.frm_penjualan;

/**
 *
 * @author Ujang Jeprut
 */
public class controllerTransaksi {
    frm_penjualan frame;
    
    ImplementTransaksi implTransaksi;
    ImplementDetailtransaksi implDTransaksi;
    List<detail_transaksi> ldt;
    List<transaksi> lb;
    Double total_harga,sisa,uang_muka;
    
    public controllerTransaksi(frm_penjualan frame)
    {
        this.frame = frame;
        implTransaksi = new daoTransaksi();
        implDTransaksi = new daoDetailtransaksi();
        lb = implTransaksi.getALL();
        ldt = implDTransaksi.getALL();
        total_harga = 0.0;
        uang_muka = 0.0;
    }
    
    public void konfig_teks()
    {
        teksModel teks_1 = new teksModel(20);
        teksModel teks_2 = new teksModel(20);
        teksModel teks_3 = new teksModel(12);
        teksModel teks_4 = new teksModel(6);
        frame.getTxt_notransaksi().setDocument(teks_1);
        frame.getTxt_namapelanggan().setDocument(teks_2);
        frame.getTxt_notelepon().setDocument(teks_3);
        frame.getTxt_kodebarang().setDocument(teks_4);
    }
    
    public void reset()
    {
        frame.getTxt_notransaksi().setText("");
        frame.getTxt_namapelanggan().setText("");
        frame.getTxt_alamat().setText("");
        frame.getTxt_notelepon().setText("");
        frame.getTxt_tanggal().setDate(null);
        frame.getTxt_kodebarang().setText("");
        frame.getTxt_ukuran().setText("");
        frame.getTxt_namabarang().setText("");
        frame.getTxt_hargasatuan().setText("");
        frame.getTxt_qty().setText("");
        frame.getTxt_jumlah().setText("");
        frame.getTxt_totalharga().setText("");
        frame.getTxt_uangmuka().setText("");
        frame.getTxt_sisa().setText("");
        frame.getTxt_cari().setText("");
        
        frame.getTxt_notransaksi().setEditable(false);
        frame.getTxt_namapelanggan().setEnabled(false);
        frame.getTxt_alamat().setEnabled(false);
        frame.getTxt_notelepon().setEnabled(false);
        frame.getTxt_tanggal().setEnabled(false);
        frame.getTxt_kodebarang().setEnabled(false);
        frame.getTxt_ukuran().setEnabled(false);
        frame.getTxt_namabarang().setEnabled(false);
        frame.getTxt_hargasatuan().setEnabled(false);
        frame.getTxt_qty().setEnabled(false);
        frame.getTxt_jumlah().setEnabled(false);
        frame.getTxt_totalharga().setEnabled(false);
        frame.getTxt_uangmuka().setEnabled(false);
        frame.getTxt_sisa().setEnabled(false);
        frame.getTxt_cari().setEnabled(true);
        frame.getT_detailtransaksi().setEnabled(false);
        
        frame.getTxt_kodebarang().setEditable(false);
        
        frame.getBtn_tambah().setEnabled(true);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_detailsimpan().setEnabled(false);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_detailhapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(false);
        frame.getBtn_keluar().setEnabled(true);
        
        
    }
    
    //fungsi untuk tombol tambah
    public void tambah()
    {
        frame.getTxt_notransaksi().setEditable(false);
        frame.getTxt_namapelanggan().setEnabled(true);
        frame.getTxt_alamat().setEnabled(true);
        frame.getTxt_notelepon().setEnabled(true);
        frame.getTxt_tanggal().setEnabled(true);
        frame.getTxt_kodebarang().setEnabled(true);
        frame.getTxt_ukuran().setEnabled(true);
        frame.getTxt_namabarang().setEnabled(true);
        frame.getTxt_hargasatuan().setEnabled(true);
        frame.getTxt_qty().setEnabled(true);
        frame.getTxt_jumlah().setEnabled(true);
        frame.getTxt_totalharga().setEnabled(true);
        frame.getTxt_uangmuka().setEnabled(true);
        frame.getTxt_sisa().setEnabled(true);
        frame.getT_detailtransaksi().setEnabled(true);

        

        frame.getTxt_cari().setText("");
        
        frame.getTxt_cari().setEnabled(false);
        
        frame.getTxt_kodebarang().requestFocus();
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(true);
        frame.getBtn_detailsimpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(false);
        frame.getBtn_hapus().setEnabled(false);
        frame.getBtn_detailhapus().setEnabled(false);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
        
        isiNoTransaksi();
        
        frame.getTxt_totalharga().setText("0");
        frame.getTxt_uangmuka().setText("0");
        frame.getTxt_sisa().setText("0");
    }
    
    public void tabelt_click()
    {
        frame.getTxt_notransaksi().setEditable(false);
        frame.getTxt_namapelanggan().setEnabled(true);
        frame.getTxt_alamat().setEnabled(true);
        frame.getTxt_notelepon().setEnabled(true);
        frame.getTxt_tanggal().setEnabled(true);
        frame.getTxt_kodebarang().setEnabled(true);
        frame.getTxt_ukuran().setEnabled(true);
        frame.getTxt_namabarang().setEnabled(true);
        frame.getTxt_hargasatuan().setEnabled(true);
        frame.getTxt_qty().setEnabled(true);
        frame.getTxt_jumlah().setEnabled(true);
        frame.getTxt_totalharga().setEnabled(true);
        frame.getTxt_uangmuka().setEnabled(true);
        frame.getTxt_sisa().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_detailsimpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(true);
        frame.getBtn_hapus().setEnabled(true);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
        frame.getBtn_detailsimpan().setEnabled(true);
    }
    
    public void tabeldt_click()
    {
        frame.getTxt_kodebarang().setEditable(true);
        frame.getTxt_ukuran().setEnabled(true);
        frame.getTxt_namabarang().setEnabled(true);
        frame.getTxt_hargasatuan().setEnabled(true);
        frame.getTxt_qty().setEnabled(true);
        frame.getTxt_jumlah().setEnabled(true);
        frame.getTxt_totalharga().setEnabled(true);
        
        frame.getBtn_tambah().setEnabled(false);
        frame.getBtn_simpan().setEnabled(false);
        frame.getBtn_detailsimpan().setEnabled(true);
        frame.getBtn_edit().setEnabled(true);
        frame.getBtn_hapus().setEnabled(true);
        frame.getBtn_detailhapus().setEnabled(true);
        frame.getBtn_batal().setEnabled(true);
        frame.getBtn_keluar().setEnabled(false);
        frame.getBtn_detailsimpan().setEnabled(true);
    }
    
    public void simpantransaksi_aksi()
    {
        frame.getTxt_kodebarang().setText("");
        frame.getTxt_ukuran().setText("");
        frame.getTxt_namabarang().setText("");
        frame.getTxt_hargasatuan().setText("");
        frame.getTxt_qty().setText("");
        frame.getTxt_jumlah().setText("");
    }
    
    public void isiNoTransaksi()
    {
        Date no_transaksi = new Date();
        String jam;
        String tanggal;
        tanggal = (String.format("%1$tm%1$td%1$tY",no_transaksi));
        jam = (String.format("%1$tH%1$tM%1$tS",no_transaksi));
        frame.getTxt_notransaksi().setText(tanggal+jam);
    }
    
    public void simpan()
    {
        if((!frame.getTxt_kodebarang().getText().trim().isEmpty()) && 
                (!frame.getTxt_ukuran().getText().trim().isEmpty()) &&
                (!frame.getTxt_hargasatuan().getText().trim().isEmpty()) &&
                (!frame.getTxt_qty().getText().trim().isEmpty()) && 
                (!frame.getTxt_jumlah().getText().trim().isEmpty()) && 
                (!frame.getTxt_namabarang().getText().trim().isEmpty()) &&
                (!frame.getTxt_notransaksi().getText().trim().isEmpty()) &&
                (!frame.getTxt_namapelanggan().getText().trim().isEmpty()) &&
                (!frame.getTxt_alamat().getText().trim().isEmpty()) &&
                (!frame.getTxt_notelepon().getText().trim().isEmpty()))
        {
            total_harga = total_harga + Double.valueOf(frame.getTxt_jumlah().getText());
            frame.getTxt_totalharga().setText(String.valueOf(total_harga));
            sisa = total_harga-uang_muka;
            frame.getTxt_sisa().setText(String.valueOf(sisa));
            simpan_transaksi();
            simpan_dtransaksi();
        }
        else
        {
            total_harga = Double.valueOf(frame.getTxt_totalharga().getText());
            frame.getTxt_totalharga().setText(String.valueOf(total_harga));
            frame.getTxt_sisa().setText(String.valueOf(total_harga));
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    public void simpan_dtransaksi()
    {
        String ukuran;
        Double jumlah,harga_satuan;
        Integer qty;
        
            detail_transaksi b = new detail_transaksi();
            b.setNo_transaksi(frame.getTxt_notransaksi().getText());
            b.setKode_barang(frame.getTxt_kodebarang().getText());
            
            b.setUkuran(frame.getTxt_ukuran().getText());
            
            harga_satuan = Double.valueOf(frame.getTxt_hargasatuan().getText());
            b.setHarga_satuan(harga_satuan);
            qty = Integer.valueOf(frame.getTxt_qty().getText());
            b.setQty(qty);
            jumlah = harga_satuan * qty;
            b.setJumlah(jumlah);
            
            implDTransaksi.insert(b);
            
            //reset();
            caridataDTransasksi();
        
    }
    
    public void simpan_transaksi()
    {
        SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
        
            transaksi b = new transaksi();
            b.setNo_transaksi(frame.getTxt_notransaksi().getText());
            b.setNama_pelanggan(frame.getTxt_namapelanggan().getText());
            b.setAlamat(frame.getTxt_alamat().getText());
            b.setNo_telp(frame.getTxt_notelepon().getText());
            
            b.setTanggal(String.valueOf(fm.format(frame.getTxt_tanggal().getDate())));
            
            b.setTotal_harga(Double.valueOf(frame.getTxt_totalharga().getText()));
            b.setBayar(Double.valueOf(frame.getTxt_uangmuka().getText()));
            b.setSisa(Double.valueOf(frame.getTxt_sisa().getText()));
            implTransaksi.insert(b);
            
            //reset();    
    }
    
    public void ubah_transaksi() 
    {
        SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
        transaksi b = new transaksi();
        b.setNo_transaksi(frame.getTxt_notransaksi().getText());
        b.setNama_pelanggan(frame.getTxt_namapelanggan().getText());
        b.setAlamat(frame.getTxt_alamat().getText());
        b.setNo_telp(frame.getTxt_notelepon().getText());
            
        b.setTanggal(String.valueOf(fm.format(frame.getTxt_tanggal().getDate())));
            
        b.setTotal_harga(Double.valueOf(frame.getTxt_totalharga().getText()));
        b.setBayar(Double.valueOf(frame.getTxt_uangmuka().getText()));
        b.setSisa(Double.valueOf(frame.getTxt_sisa().getText()));
        
        implTransaksi.update(b);
        JOptionPane.showMessageDialog(null, "Data Berhasil DiUbah",
                "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
        reset();
    }
    
    public void hapus()
    {
        int n = JOptionPane.showConfirmDialog(
            null,
            "Apakah Anda yakin ingin menghapus data ini ?",
            "Konfirmasi",
            JOptionPane.YES_NO_OPTION);
        
        if(n == JOptionPane.YES_OPTION)
        {
        implTransaksi.delete(frame.getTxt_notransaksi().getText());
        JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus",
                    "Infofmasi",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    public void batal()
    {
        implTransaksi.delete(frame.getTxt_notransaksi().getText());
    }
    
    public void isiFieldTransaksi(int row)
    {
        SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
        Date tanggal;
        frame.getTxt_notransaksi().setText(lb.get(row).getNo_transaksi().toString());
        frame.getTxt_namapelanggan().setText(lb.get(row).getNama_pelanggan().toString());
        frame.getTxt_alamat().setText(lb.get(row).getAlamat().toString());
        frame.getTxt_notelepon().setText(lb.get(row).getNo_telp().toString());
        try {
            tanggal = fm.parse(lb.get(row).getTanggal().toString());
            frame.getTxt_tanggal().setDate(tanggal);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        frame.getTxt_totalharga().setText(lb.get(row).getTotal_harga().toString());
        frame.getTxt_uangmuka().setText(lb.get(row).getBayar().toString());
        frame.getTxt_sisa().setText(lb.get(row).getSisa().toString());
    }
    
    //menampilkan isi data ke dalam tabel
    public void isiTableTransaksi()
    {
        lb = implTransaksi.getALL();
        tableModelTransaksi tmb = new tableModelTransaksi(lb);
        frame.getT_transaksi().setModel(tmb);
    }
    
    //menampilkan isi data ke dalam tabel
    public void isiTableDTransaksi()
    {
        ldt = implDTransaksi.getALL();
        tableModelDetailTransaksi tmb = new tableModelDetailTransaksi(ldt);
        frame.getT_detailtransaksi().setModel(tmb);
    }
    
    
    public void isiTableCariDataDTransaksi() 
    {
        ldt = implDTransaksi.getCariData(frame.getTxt_notransaksi().getText());
        tableModelDetailTransaksi tmb = new tableModelDetailTransaksi(ldt);
        frame.getT_detailtransaksi().setModel(tmb);
    }
    
    public void caridataDTransasksi()
    {
        if (frame.getTxt_notransaksi().getText().trim().isEmpty())
        {
            isiTableDTransaksi();
        }
        else
        {
            implDTransaksi.getCariData(frame.getTxt_notransaksi().getText());
            isiTableCariDataDTransaksi();
        }            
    }
    
}
