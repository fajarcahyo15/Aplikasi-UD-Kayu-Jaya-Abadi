/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

/**
 *
 * @author hikira1
 */
public class barang {
    private String kode_barang;
    private String kode_model;
    private String nama_model;
    private String kode_kayu;
    private String jenis_kayu;
    private Double harga_kayu;
    private String nama_barang;
    private Double harga_barang;

    public Double getHarga_kayu() {
        return harga_kayu;
    }

    public void setHarga_kayu(Double harga_kayu) {
        this.harga_kayu = harga_kayu;
    }

    public String getNama_model() {
        return nama_model;
    }

    public void setNama_model(String nama_model) {
        this.nama_model = nama_model;
    }

    public String getJenis_kayu() {
        return jenis_kayu;
    }

    public void setJenis_kayu(String jenis_kayu) {
        this.jenis_kayu = jenis_kayu;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getKode_model() {
        return kode_model;
    }

    public void setKode_model(String kode_model) {
        this.kode_model = kode_model;
    }

    public String getKode_kayu() {
        return kode_kayu;
    }

    public void setKode_kayu(String kode_kayu) {
        this.kode_kayu = kode_kayu;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public Double getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(Double harga_barang) {
        this.harga_barang = harga_barang;
    }
}
