/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hikira1
 */
public class tableModelBarang extends AbstractTableModel {
    List<barang> lb;

    public tableModelBarang(List<barang> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Kode Barang";
            case 1:
                return "Kode Model";
            case 2:
                return "Nama Model";
            case 3:
                return "Kode Kayu";
            case 4:
                return "Jenis Kayu";
            case 5:
                return "Harga Kayu";
            case 6:
                return "Nama Barang";
            case 7:
                return "Harga";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getKode_barang();
            case 1:
                return lb.get(row).getKode_model();
            case 2:
                return lb.get(row).getNama_model();
            case 3:
                return lb.get(row).getKode_kayu();
            case 4:
                return lb.get(row).getJenis_kayu();
            case 5:
                return lb.get(row).getHarga_kayu();
            case 6:
                return lb.get(row).getNama_barang();
            case 7:
                return lb.get(row).getHarga_barang();
            default:
                return null;
        }
    }
}
