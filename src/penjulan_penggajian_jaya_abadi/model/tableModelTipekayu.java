/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hikira1
 */
public class tableModelTipekayu extends AbstractTableModel {
    List<tipekayu> lb;

    public tableModelTipekayu(List<tipekayu> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Kode Kayu";
            case 1:
                return "Jenis Kayu";
            case 2:
                return "Harga";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getKode_kayu();
            case 1:
                return lb.get(row).getJenis_kayu();
            case 2:
                return lb.get(row).getHarga();
            default:
                return null;
        }
    }
    
}
