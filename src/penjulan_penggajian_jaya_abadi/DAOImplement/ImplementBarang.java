/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.DAOImplement;

import java.util.List;
import penjulan_penggajian_jaya_abadi.model.*;

/**
 *
 * @author Ujang Jeprut
 */
public interface ImplementBarang {
    public void insert(barang b);
    
    public void update(barang b);

    public void delete(String kode_barang);

    public List<barang> getALL();

    public List<barang> getCariData(String data);
}
