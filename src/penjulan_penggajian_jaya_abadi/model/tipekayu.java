/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

/**
 *
 * @author hikira1
 */
public class tipekayu {
    private String kode_kayu;
    private String jenis_kayu;
    private Double harga;

    public String getKode_kayu() {
        return kode_kayu;
    }

    public void setKode_kayu(String kode_kayu) {
        this.kode_kayu = kode_kayu;
    }

    public String getJenis_kayu() {
        return jenis_kayu;
    }

    public void setJenis_kayu(String jenis_kayu) {
        this.jenis_kayu = jenis_kayu;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }
}
