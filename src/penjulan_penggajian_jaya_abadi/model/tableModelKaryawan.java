/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penjulan_penggajian_jaya_abadi.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hikira1
 */
public class tableModelKaryawan extends AbstractTableModel {
    List<karyawan> lb;

    public tableModelKaryawan(List<karyawan> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID Karyawan";
            case 1:
                return "Nama Karyawan";
            case 2:
                return "Alamat";
            case 3:
                return "No. HP";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getId_karyawan();
            case 1:
                return lb.get(row).getNama_karyawan();
            case 2:
                return lb.get(row).getAlamat();
            case 3:
                return lb.get(row).getNo_hp();
            default:
                return null;
        }
    }
}
